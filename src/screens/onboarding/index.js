// Core Packages
import React, { useContext, useCallback, useEffect, useState } from 'react';

// Custom Component

import Button from '@components/button';
import { OnBoardHead } from '@components/onboardHead';
import { AccountCard } from '@components/accountCard';
import Input from '@components/input';
import { Loader } from '@components/loader';
import { IMAGES } from '@utils/images';

// Utils
import { ANIMATIONS } from '@assets/animations';
import { AuthContext } from '@contexts/auth';
import { ROUTE_NAMES } from '@navigation/route-names';
import { ProgressBar } from 'react-native-stories-view';
import {
  Image,
  StatusBar,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { useIsFocused } from '@react-navigation/native';
import {
  Wrapper,
  GroupView,
  PurpleHeader,
  DateText,
  WelcomeText,
  WelcomeThree,
} from './styles';
import { COLORS } from '@utils/theme';

const windowDimensions = Dimensions.get('window');

export const OnBoarding = ({ navigation }) => {
  const [showBanner, setShowBanner] = useState(true);
  const accountData = [
    {
      accountType: 'PROSPA CURRENT ACCOUNT',
      accountNumber: '00-00-00 18361554',
      accountBalance: '5,234.96',
      imagesSource: IMAGES.WALLET,
    },
    {
      accountType: 'SAVINGS ACCOUNT',
      accountNumber: '00-00-00 18361554',
      accountBalance: '3,234.96',
      imagesSource: IMAGES.SAVINGS,
    },
    {
      accountType: 'TAX ACCOUNT',
      accountNumber: '00-00-00 18361554',
      accountBalance: '10,234.96',
      imagesSource: IMAGES.TAX,
    },
  ];

  const loopedData = accountData.map((items, index) => (
    <AccountCard
      key={index}
      source={items.imagesSource}
      accountType={items.accountType}
      accountNumber={items.accountNumber}
      accountBalance={items.accountBalance}
      onPress={() => navigation.navigate(ROUTE_NAMES.CURRENT_ACCOUNT)}
    />
  ));

  return (
    <Wrapper>
      <GroupView>
        <OnBoardHead
          imageLink={IMAGES.INFO}
          title={''}
          subTitle={''}
          RHS_children={
            <Image
              source={IMAGES.Img1}
              style={{ width: 40, height: 40, borderRadius: 40 / 2 }}
            />
          }
        />
        <PurpleHeader>
          <DateText>THURSDAY, 18 JUNE</DateText>
          <WelcomeText>{'Welcome back, \n Olivia!'}</WelcomeText>
          <View
            style={{
              width: '16%',
              height: 60,
              position: 'absolute',
              bottom: -28,
              left: '47.2%',
              zIndex: 99999,
              borderRadius: 35,
              backgroundColor: '#270450',
              transform: [{ scaleX: 7 }, { scaleY: 0.5 }],
            }}
          />
        </PurpleHeader>
        <View style={{ flex: 1, marginTop: -50, minHeight: 400, padding: 20 }}>
          {loopedData}
          {showBanner === true && (
            <TouchableOpacity onPress={() => setShowBanner(!showBanner)}>
              <Image
                source={IMAGES.BANNER}
                style={{ width: '100%', height: 80 }}
              />
            </TouchableOpacity>
          )}
        </View>
      </GroupView>
    </Wrapper>
  );
};
