// Core Packages
import React, { useContext, useCallback, useEffect } from "react";

// Custom Component
import Button from "@components/button";
import { OnBoardHead } from "@components/onboardHead";
import Input from "@components/input";
import {Otp} from "@components/otp";
import { IMAGES } from "@utils/images";

// Utils

import { ROUTE_NAMES } from "@navigation/route-names";
import { Wrapper, GroupView } from "./styles";
import { COLORS } from "@utils/theme";

export const SetPin = ({ navigation }) => {
  useEffect(() => {
    // ;
  }, []);

  return (
    <Otp
      title={"Set PIN"}
      routeTo={ROUTE_NAMES.CONFIRM_PIN}
      navigation={navigation}
      subTitle={"Facilisis mauris, potenti vitae cras risus."}
    />
  );
};
