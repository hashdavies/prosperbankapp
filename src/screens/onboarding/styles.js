// Core Packages
import styled from "styled-components/native";
import { COLORS } from "@utils/theme";

export const Wrapper = styled.View`
  flex: 1;
  justify-content: space-between;
  align-items: center;
  background: ${COLORS.offWhite};
  width: 100%;
`;
export const GroupView = styled.View`
  display: flex;
  width: 100%;
`;
export const GroupViewer = styled.View`
  display: flex;
  width: 100%;
`;
export const VerifyText = styled.Text`
  font-family: "ABeeZee";
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 14px;
  text-align: center;
  margin-top: 20px;
  margin-bottom: 8px;

  color: ${COLORS.fadeDark2};
`;
export const CounterText = styled.Text`
  font-family: "ABeeZee";
  font-style: italic;
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  text-align: center;
  color: ${COLORS.btnDark};
`;
export const PurpleHeader = styled.View`
  background: ${COLORS.purple};
  padding-top: 20px;
  padding-bottom: 90px;
  padding-left:  20px;
  padding-right:  20px;
  // border-bottom-right-radius: 10px;
  // border-bottom-left-radius: 10px;
`;
export const DateText = styled.Text`
  font-family: "ABeeZee";
  font-weight: 400;
  font-size: 11px;
  color: #9CA0A5;

`;
export const WelcomeText = styled.Text`
  font-family: "ABeeZee";
  font-size: 28px;
  color: ${COLORS.white};
`;
export const WelcomeThree = styled.Text`
  font-family: "ABeeZee";
  font-size: 28px;
  color: ${COLORS.grey};
`;
