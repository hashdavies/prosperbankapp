// Core Packages
import React, { useContext, useCallback, useEffect } from "react";

// Custom Component
import Slider from "./slider";
import SliderItem from "./slider/slider-item";
import Button from "@components/button";
import { OnBoardHead } from "@components/onboardHead";
import Input from "@components/input";
import { PinInput } from "@components/pin-input";
import { Loader } from "@components/loader";

// Utils
import { ANIMATIONS } from "@assets/animations";
import { AuthContext } from "@contexts/auth";
import { ROUTE_NAMES } from "@navigation/route-names";
import { ProgressBar } from "react-native-stories-view";
import { StatusBar } from "react-native";
import { useIsFocused } from "@react-navigation/native";
import { Wrapper, GroupView, VerifyText, CounterText, GroupViewer } from "./styles";
import { COLORS } from "@utils/theme";
import { IMAGES } from "@utils/images";

export const Verify = ({ navigation }) => {
  useEffect(() => {
    // ;
  }, []);

  return (
    <Wrapper>
      <GroupView>
        <OnBoardHead
          imageLink={IMAGES.ARROW_LEFT}
          title={"Verification"}
          subTitle={"Lorem ipsum dolor sit amet, consectetur adipiscing elit."}
        />
        <PinInput size={4} pin={4} />
        <VerifyText>Verification code will be resent in</VerifyText>
        <GroupViewer><CounterText>0:54</CounterText></GroupViewer>
      </GroupView>
      <Button
        label={"Submit"}
        onPress={() => navigation.navigate(ROUTE_NAMES.BVN)}
      />
    </Wrapper>
  );
};
