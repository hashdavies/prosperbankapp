// Core Packages
import styled from "styled-components/native";
import { COLORS } from "@utils/theme";

export const Wrapper = styled.View`
  flex: 1;
  justify-content: space-between;
  align-items: center;
  background: ${COLORS.offWhite};
  width: 100%;
`;
export const GroupView = styled.View`
  display: flex;
  width: 100%;
`;
export const FullFlex = styled.View`
  flex-direction: row;
  width: 100%;
  justify-content: space-between
  align-items: center
`;
export const ActionWrap = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
`;
export const Image = styled.Image`
  width: 24px;
  height: 24px;
`;
export const GroupViewer = styled.View`
  display: flex;
  width: 100%;
`;
export const FlexCenter = styled.View`
  justify-content: center;
  align-items: center;
  background-color: #270450;
  height: 100%;
`;

export const MoneyText = styled.Text`
  font-family: "ABeeZee";
  font-style: normal;
  font-weight: normal;
  font-size: 15px;
  line-height: 17px;
  text-align: center;
  margin-top: 12px;
  margin-bottom: 8px;
  color: #fa4a84;
`;
export const CarouselLabel = styled.Text`
  font-style: normal;
  font-weight: normal;
  font-size: 20px;
  line-height: 18px;
  text-align: center;
  margin-bottom: 2px;
  color: #ffffff;
`;
export const CarouselAccountNumber = styled.Text`
  font-style: normal;
  font-weight: normal;
  font-size: 15px;
  line-height: 15px;
  text-align: center;
  margin-bottom: 15px;
  color: #ffffff;
  opacity: 0.6
`;
export const CarouselAmount = styled.Text`
  font-style: normal;
  font-weight: normal;
  font-size: 40px;
  line-height: 52px;
  text-align: center;
  margin-top: 25px;
  margin-bottom: 10px;
  color: #ffffff;
`;

export const VerifyText = styled.Text`
  font-family: "ABeeZee";
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 14px;
  text-align: center;
  margin-top: 20px;
  margin-bottom: 8px;

  color: ${COLORS.fadeDark2};
`;
export const CounterText = styled.Text`
  font-family: "ABeeZee";
  font-style: italic;
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  text-align: center;
  color: ${COLORS.btnDark};
`;
export const PinkHeader = styled.View`
  background: ${COLORS.pink};
  padding-top: 20px;
  // padding-bottom: 90px;
  padding-left: 20px;
  padding-right: 20px;
`;
export const DateText = styled.Text`
  font-family: "ABeeZee";
  font-weight: 400;
  font-size: 14px;
  color: #9ca0a5;
`;
export const SubText = styled.Text`
  font-family: "ABeeZee";
  font-weight: 400;
  font-size: 11px;
  color: #9ca0a5;
`;
export const WelcomeText = styled.Text`
  font-family: "ABeeZee";
  font-size: 22px;
  color: ${COLORS.black};
  margin-top: 10px;
  margin-bottom: 30px;
`;
export const HeaderText = styled.Text`
  font-family: "ABeeZee";
  font-size: 22px;
  color: ${COLORS.black};
  margin-top: 10px;
  margin-bottom: 3px;
`;
export const HeaderSideText = styled.Text`
  font-family: "ABeeZee";
  font-size: 12px;
  color: #fa4a84;
  line-height: 16px;
`;
export const WelcomeThree = styled.Text`
  font-family: "ABeeZee";
  font-size: 28px;
  color: ${COLORS.grey};
`;
export const Circle = styled.View`
  width: 70px;
  height: 70px;
  background-color: ${COLORS.white};
  margin-top: -35px;
  margin-left: 20px;
  justify-content: center;
  align-items: center;
`;
export const MoneyOutIn = styled.View`
  min-height: 200px;
  padding: 20px;
  padding-right: 0px;
  padding-top: 25px;
  justify-content: center;
  align-items: center;
  background-color: #ffffff;
`;
export const CaretImage = styled.Image`
  height: 8px;
  width: 12px;
`;
export const Menu = styled.Image`
  height: 50px;
  width: 50px;
`;
export const Vertical = styled.Image`
  height: 51px;
  width: 8px;
  background-color: #5d6262;
  border-radius: 2px;
`;
