// Core Packages
import React, { useContext, useCallback, useEffect, useState } from 'react';

// Custom Component
// import Slider from "./slider";
// import SliderItem from "./slider/slider-item";
import Button from '@components/button';
import { OnBoardHead } from '@components/onboardHead';
import { CarouselComp } from '@components/carousel';
import SimpleCarousel from '@components/carousel/newca';
import { AccountCard } from '@components/accountCard';
import { ListCard } from '@components/listCard';
import { TransactionCard } from '@components/listCard/transactionCard';
import Input from '@components/input';
import { Loader } from '@components/loader';
import { IMAGES } from '@utils/images';

// Utils
import { ANIMATIONS } from '@assets/animations';
import { AuthContext } from '@contexts/auth';
import { ROUTE_NAMES } from '@navigation/route-names';
import { ProgressBar } from 'react-native-stories-view';
import Animated, {
  Extrapolate,
  interpolate,
  useAnimatedStyle,
  useSharedValue,
} from 'react-native-reanimated';
import {
  // Image,
  StatusBar,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Text,
} from 'react-native';
import { useIsFocused } from '@react-navigation/native';
import {
  Wrapper,
  GroupView,
  PinkHeader,
  DateText,
  WelcomeText,
  HeaderSideText,
  Circle,
  HeaderText,
  SubText,
  FullFlex,
  MoneyOutIn,
  MoneyText,
  CaretImage,
  Vertical,
  Menu,
  FlexCenter,
  CarouselLabel,
  CarouselAccountNumber,
  CarouselAmount,
  ActionWrap,
  Image,
} from './styles';
import { COLORS } from '@utils/theme';
// import { ScrollView } from "node_modules/react-native-gesture-handler/lib/typescript/index";

const windowDimensions = Dimensions.get('window');

export const CurrentAccount = ({ navigation }) => {
  const [showBanner, setShowBanner] = useState(true);
  const progressValue = useSharedValue(0);

  const accountData = [
    {
      accountType: 'PROSPA CURRENT ACCOUNT',
      accountNumber: '00-00-00 18361554',
      accountBalance: '5,234.96',
      imagesSource: IMAGES.WALLET,
    },
    {
      accountType: 'SAVINGS ACCOUNT',
      accountNumber: '00-00-00 18361554',
      accountBalance: '3,234.96',
      imagesSource: IMAGES.SAVINGS,
    },
    {
      accountType: 'TAX ACCOUNT',
      accountNumber: '00-00-00 18361554',
      accountBalance: '10,234.96',
      imagesSource: IMAGES.TAX,
    },
  ];
  const DataList = [
    {
      Account: 'Current Account',
      accountNumber: '00-00 18361554',
      accountBalance: '₦905,234.96',
    },
    {
      Account: 'Saving Account',
      accountNumber: '00-00 18361554',
      accountBalance: '₦520,234.96',
    },
    {
      Account: 'Domiciliary Account',
      accountNumber: '00-00 18361554',
      accountBalance: '₦999,999,999,234.96',
    },
  ];
  // useEffect(() => {
  //   // ;Img1
  // }, []);

  const loopedData = accountData.map((items, index) => (
    <AccountCard
      key={index}
      source={items.imagesSource}
      accountType={items.accountType}
      accountNumber={items.accountNumber}
      accountBalance={items.accountBalance}
    />
  ));
  const PaginationItem = ({
    animValue,
    index,
    length,
    backgroundColor,
    isRotate,
  }) => {
    //   const { animValue, index, length, backgroundColor, isRotate } = props;
    const width = 10;

    const animStyle = useAnimatedStyle(() => {
      let inputRange = [index - 1, index, index + 1];
      let outputRange = [-width, 0, width];

      if (index === 0 && animValue?.value > length - 1) {
        inputRange = [length - 1, length, length + 1];
        outputRange = [-width, 0, width];
      }

      return {
        transform: [
          {
            translateX: interpolate(
              animValue?.value,
              inputRange,
              outputRange,
              Extrapolate.CLAMP,
            ),
          },
        ],
      };
    }, [animValue, index, length]);
    return (
      <View
        style={{
          backgroundColor: '#51125A',
          width,
          height: width,
          borderRadius: 50,
          overflow: 'hidden',
          transform: [
            {
              rotateZ: isRotate ? '90deg' : '0deg',
            },
          ],
        }}>
        <Animated.View
          style={[
            {
              borderRadius: 50,
              backgroundColor,
              flex: 1,
            },
            animStyle,
          ]}
        />
      </View>
    );
  };

  return (
    <Wrapper>
      <ScrollView style={{ width: '100%' }}>
        <GroupView>
          <View
            style={{
              width: windowDimensions.width,
              backgroundColor: '#270450',
              marginBottom: 10,
            }}>
            <CarouselComp
              contentElement={({ item }) => (
                <FlexCenter>
                  <CarouselLabel>{item.Account}</CarouselLabel>
                  <CarouselAccountNumber>
                    {item.accountNumber}
                  </CarouselAccountNumber>
                  <CarouselAmount>{item.accountBalance}</CarouselAmount>
                </FlexCenter>
              )}
              data={DataList}
              width={windowDimensions.width}
              height={200}
              onProgressChange={(_, absoluteProgress) =>
                (progressValue.value = absoluteProgress)
              }
            />
            <View
              style={{
                justifyContent: 'center',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              {DataList.map((backgroundColor, index) => {
                return (
                  <PaginationItem
                    backgroundColor={'#FA4A84'}
                    animValue={progressValue}
                    index={index}
                    key={index}
                    isRotate={true}
                    length={DataList.length}
                  />
                );
              })}
            </View>

            <FullFlex
              style={{
                position: 'absolute',
                top: 20,
                left: 0,
                right: 0,
                paddingRight: 18,
                paddingLeft: 18,
              }}>
              <ActionWrap
                onPress={() => navigation.navigate(ROUTE_NAMES.ON_BOARDING)}>
                <Image source={IMAGES.arrowBack} />
              </ActionWrap>
              <HeaderSideText>Options</HeaderSideText>
            </FullFlex>
            <View
              style={{
                width: '16%',
                height: 60,
                position: 'absolute',
                bottom: -25,
                left: '42%',
                zIndex: 99999,
                borderRadius: 35,
                backgroundColor: '#270450',
                transform: [{ scaleX: 7 }, { scaleY: 0.5 }],
              }}
            />
          </View>

          <FullFlex
            style={{
              alignItems: 'flex-end',
              justifyContent: 'space-around',
              paddingTop: 20,
              backgroundColor: '#FFFFFF',
            }}>
            <View>
              <Menu source={IMAGES.transfer} />
              <Text
                style={{
                  fontSize: 13,
                  color: '#5D6262',
                  textAlign: 'center',
                  marginTop: 6,
                }}>
                Transfer
              </Text>
            </View>
            <View>
              <Menu source={IMAGES.transfer} />
              <Text
                style={{
                  fontSize: 13,
                  color: '#5D6262',
                  textAlign: 'center',
                  marginTop: 6,
                }}>
                PAY
              </Text>
            </View>
            <View>
              <Menu source={IMAGES.transfer} />
              <Text
                style={{
                  fontSize: 13,
                  color: '#5D6262',
                  textAlign: 'center',
                  marginTop: 6,
                }}>
                card
              </Text>
            </View>
          </FullFlex>
          <MoneyOutIn>
            <View
              style={{
                minHeight: 126,
                width: '90%',
                padding: 20,
                // paddingRight: 0,
                backgroundColor: COLORS.white,
                paddingTop: 25,
                elevation: 1,
                borderRadius: 10,
              }}>
              <FullFlex>
                <View />
                <MoneyText>Last 30 days</MoneyText>
                <CaretImage source={IMAGES.caretRight} />
              </FullFlex>

              <FullFlex style={{ alignItems: 'flex-end' }}>
                <View>
                  <Text
                    style={{
                      fontSize: 20,
                      color: '#00AF3F',
                      textAlign: 'center',
                    }}>
                    ₦5,250.00
                  </Text>
                  <Text
                    style={{
                      fontSize: 13,
                      color: '#9CA0A5',
                      textAlign: 'center',
                    }}>
                    Money in
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'flex-end',
                    marginTop: 27,
                  }}>
                  <Vertical
                    style={{
                      height: 41,
                      backgroundColor: '#3CBA54',
                      marginRight: 3,
                    }}
                  />
                  <Vertical />
                </View>
                <View>
                  <Text style={{ fontSize: 20, color: '#5D6262' }}>
                    ₦5,250.00
                  </Text>
                  <Text
                    style={{
                      fontSize: 13,
                      color: '#9CA0A5',
                      textAlign: 'center',
                    }}>
                    Money out
                  </Text>
                </View>
              </FullFlex>
            </View>
          </MoneyOutIn>

          <View
            style={{
              padding: 20,
            }}>
            <DateText>WED, 3 JULY</DateText>
          </View>
          <View
            style={{
              minHeight: 300,
              padding: 20,
              paddingRight: 0,
              backgroundColor: COLORS.white,
              paddingTop: 25,
              elevation: 1,
            }}>
            <TransactionCard
              imgSource={IMAGES.airway}
              listTitle={'British Airways'}
              miniText={'Travel'}
              imgHeight={35}
              imgWidth={35}
              transactionValue={'500.00'}
              MiniTextColor={'#FA4A84'}
              onPress={() => navigation.navigate(ROUTE_NAMES.HISTORY)}
              status={true}
            />
            <TransactionCard
              imgSource={IMAGES.mobile}
              listTitle={'MTN Mobile'}
              miniText={'Utility'}
              imgHeight={35}
              imgWidth={35}
              transactionValue={'250.79'}
              MiniTextColor={'#BB53F2'}
              status={false}
            />

            <TransactionCard
              imgSource={IMAGES.invoice}
              listTitle={'Invoice #0044'}
              miniText={'Sales'}
              imgHeight={35}
              imgWidth={35}
              transactionValue={'1,050.00'}
              MiniTextColor={'#7E51FF'}
              status={true}
            />
            <TransactionCard
              imgSource={IMAGES.user}
              listTitle={'Christina Rose'}
              miniText={'Salary'}
              imgHeight={35}
              imgWidth={35}
              transactionValue={'250,000.00'}
              MiniTextColor={'#74D4F0'}
              status={false}
            />
          </View>
        </GroupView>
        {/* <Button
        label={"Get Started"}
        onPress={() => navigation.navigate(ROUTE_NAMES.VERIFY)}
      /> */}
      </ScrollView>
    </Wrapper>
  );
};
