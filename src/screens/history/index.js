// Core Packages
import React, { useContext, useCallback, useEffect, useState } from "react";

// Custom Component
// import Slider from "./slider";
// import SliderItem from "./slider/slider-item";
import Button from "@components/button";
import { OnBoardHead } from "@components/onboardHead";
import { AccountCard } from "@components/accountCard";
import { ListCard } from "@components/listCard";
import Input from "@components/input";
import { Loader } from "@components/loader";
import { IMAGES } from "@utils/images";

// Utils
import { ANIMATIONS } from "@assets/animations";
import { AuthContext } from "@contexts/auth";
import { ROUTE_NAMES } from "@navigation/route-names";
import { ProgressBar } from "react-native-stories-view";
import {
  Image,
  StatusBar,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Text,
  Switch,
} from "react-native";
import { useIsFocused } from "@react-navigation/native";
import {
  Wrapper,
  GroupView,
  PinkHeader,
  DateText,
  WelcomeText,
  HeaderSideText,
  Circle,
  HeaderText,
  SubText,
} from "./styles";
import { COLORS } from "@utils/theme";
// import { ScrollView } from "node_modules/react-native-gesture-handler/lib/typescript/index";

const windowDimensions = Dimensions.get("window");

export const History = ({ navigation }) => {
  const [showBanner, setShowBanner] = useState(true);
  const [isEnabled, setIsEnabled] = useState(false);
  
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);
  const accountData = [
    {
      accountType: "PROSPA CURRENT ACCOUNT",
      accountNumber: "00-00-00 18361554",
      accountBalance: "5,234.96",
      imagesSource: IMAGES.WALLET,
    },
    {
      accountType: "SAVINGS ACCOUNT",
      accountNumber: "00-00-00 18361554",
      accountBalance: "3,234.96",
      imagesSource: IMAGES.SAVINGS,
    },
    {
      accountType: "TAX ACCOUNT",
      accountNumber: "00-00-00 18361554",
      accountBalance: "10,234.96",
      imagesSource: IMAGES.TAX,
    },
  ];
  // useEffect(() => {
  //   // ;Img1
  // }, []);

  const loopedData = accountData.map((items, index) => (
    <AccountCard
      key={index}
      source={items.imagesSource}
      accountType={items.accountType}
      accountNumber={items.accountNumber}
      accountBalance={items.accountBalance}
    />
  ));

  return (
    <Wrapper>
      <ScrollView style={{ width: "100%" }}>
        <GroupView>
          <OnBoardHead
            imageLink={IMAGES.arrowPink}
            bgColor={"#BB53F2"}
            handleOnPress={() =>
              navigation.navigate(ROUTE_NAMES.CURRENT_ACCOUNT)
            }
            title={""}
            subTitle={""}
            RHS_children={<HeaderSideText>Options</HeaderSideText>}
          />
          <PinkHeader />
          <Circle style={{ borderRadius: 35, zIndex: 9, elevation: 2 }}>
            <Image source={IMAGES.phone} style={{ width: 18, height: 32 }} />
          </Circle>
          <View
            style={{
              flex: 1,
              minHeight: 350,
              padding: 20,
              paddingRight: 0,
              backgroundColor: COLORS.white,
              marginTop: -35,
              paddingTop: 65,
              elevation: 1,
            }}
          >
            <DateText>5 JULY 2019 11:50 AM</DateText>
            <WelcomeText>MTN Mobile</WelcomeText>
            <ListCard
              imgSource={IMAGES.phone}
              listTitle={"Utility"}
              imgHeight={22}
              imgWidth={12}
            />
            <ListCard
              imgSource={IMAGES.walletSave}
              listTitle={"Current Account"}
              imgHeight={17}
              imgWidth={20}
            />
            <ListCard
              imgSource={IMAGES.nairaCard}
              listTitle={"Add a receipt"}
              imgHeight={20}
              imgWidth={15}
            />
          </View>
          <View
            style={{
              padding: 20,
            }}
          >
            <DateText>USER HISTORY</DateText>
          </View>
          <View
            style={{
              minHeight: 300,
              padding: 20,
              paddingRight: 0,
              backgroundColor: COLORS.white,
              paddingTop: 25,
              elevation: 1,
            }}
          >
            <WelcomeText>You and MTN</WelcomeText>
            <ListCard
              imgSource={IMAGES.date}
              listTitle={"June total spent"}
              imgHeight={15}
              imgWidth={17}
            />
            <ListCard
              imgSource={IMAGES.outlineWallet}
              listTitle={"Transaction in July"}
              imgHeight={14}
              imgWidth={17}
            />
            <ListCard
              imgSource={IMAGES.calculator}
              listTitle={"Average spent"}
              imgHeight={20}
              imgWidth={15}
            />
          </View>
          <View
            style={{
              padding: 20,
            }}
          >
            <DateText>SUBSCRIPTION</DateText>
          </View>

          <View
            style={{
              minHeight: 70,
              padding: 20,
              paddingRight: 0,
              backgroundColor: COLORS.white,
              paddingTop: 25,
              elevation: 1,
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <View style={{ flex: 1 }}>
              <HeaderText>Repeating payment</HeaderText>
              <SubText>Repeating payment</SubText>
            </View>
            <Switch
              trackColor={{ false: "#767577", true: "#3CBA54" }}
              thumbColor={isEnabled ? "#fff" : "#f4f3f4"}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleSwitch}
              value={isEnabled}
            />
          </View>
        </GroupView>
        {/* <Button
        label={"Get Started"}
        onPress={() => navigation.navigate(ROUTE_NAMES.VERIFY)}
      /> */}
      </ScrollView>
    </Wrapper>
  );
};
