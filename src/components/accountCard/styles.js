// Core Packages
import styled from "styled-components/native";
import { COLORS } from "@utils/theme";

export const Wrapper = styled.TouchableOpacity`
  flex-direction: row
  justify-content: space-between;
  background: ${COLORS.white};
  width: 100%;
  padding: 18px;
  border-radius: 4px;
  margin-bottom: 15px
`;
export const AccountType = styled.Text`
  font-family: "Nexa";
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 16px;
  text-align: left;
  color: #5D6262;
`;
export const AccountNumber = styled.Text`
font-family: 'ABeeZee';
font-style: normal;
font-weight: 600;
font-size: 12px;
line-height: 13px;
text-align: left;
color: #9CA0A5;
margin-top: 6px;
`;
export const AccountBalance = styled.Text`
font-family: 'ABeeZee';
font-style: normal;
font-size: 29px;
line-height: 34px;
text-align: left;
color: #270450;
`;
export const FLexTabTopImage = styled.Image`
  width: 40px;
  height: 40px;
`;
export const FlexCenter = styled.View`
  justify-content: center;
  // align-items: center;
  // width: 100%;
  flex: 1;
  // margin-bottom: 34px;
`;
