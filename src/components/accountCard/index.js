// Core Packages
import React from "react";

import {
  Wrapper,
  FLexTabTopImage,
  FlexCenter,
  AccountNumber,
  AccountType,
  AccountBalance,
} from "./styles";

export const AccountCard = ({
  source,
  accountType,
  accountNumber,
  accountBalance,
  onPress,
}) => {
  return (
    <Wrapper onPress={onPress}>
      <FlexCenter>
        <AccountType>{accountType}</AccountType>
        <AccountNumber>{accountNumber}</AccountNumber>
        <AccountBalance>{`₦${accountBalance}`}</AccountBalance>
      </FlexCenter>
      <FLexTabTopImage source={source} />
    </Wrapper>
  );
};
