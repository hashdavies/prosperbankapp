// Core Packages
import { View, Text } from '@components/action-tile/styles';
import React from 'react';
import { GestureHandlerRootView } from 'react-native-gesture-handler';

// Custom Component
import Carousel from 'react-native-reanimated-carousel';

// Utils
import {
  Wrapper,
  FLexTabTopImage,
  FlexCenter,
  WalletBalLabel,
  WalletBalValue,
} from './styles';

export const CarouselComp = ({
  contentElement,
  data,
  height,
  width,
  onProgressChange,
}) => {
  return (
    <GestureHandlerRootView>
      <Carousel
        autoPlay={true}
        autoPlayInterval={2500}
        // loop
        mode="parallax"
        modeConfig={{
          parallaxScrollingScale: 0.9,
          parallaxScrollingOffset: 50,
        }}
        onProgressChange={onProgressChange}
        pagingEnabled={true}
        snapEnabled={true}
        width={width}
        height={height}
        data={data}
        renderItem={contentElement}
      />
    </GestureHandlerRootView>
  );
};
