// Core Packages
import { IMAGES } from "@utils/images";
import React from "react";
import {View} from "react-native";

import {
  Wrapper,
  FLexTabTopImage,
  FlexCenter,
  AccountNumber,
  CaretImage,
  AccountType,
  AccountBalance,
  MiniText,
  ValueText,
} from "./styles";

export const TransactionCard = ({
  imgSource,
  listTitle,
  imgHeight,
  imgWidth,
  transactionValue,
  miniText,
  MiniTextColor,
  status,
  onPress,
}) => {
  return (
    <Wrapper onPress={onPress}>
      <FLexTabTopImage
        source={imgSource}
        style={{ width: imgWidth, height: imgHeight }}
      />
      <FlexCenter
        style={{ borderBottomWidth: 1, borderBottomColor: "#EEF0F1" }}
      >
        <View>
          <AccountType>{listTitle}</AccountType>
          <MiniText style={{ color: MiniTextColor }}>{miniText}</MiniText>
        </View>
        {status === true ? (
          <ValueText
            style={{ color: "#3CBA54" }}
          >{`₦${transactionValue}`}</ValueText>
        ) : (
          <ValueText
            style={{ color: "#270450" }}
          >{`₦${transactionValue}`}</ValueText>
        )}
      </FlexCenter>
    </Wrapper>
  );
};
