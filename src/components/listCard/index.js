// Core Packages
import { IMAGES } from "@utils/images";
import React from "react";

import {
  Wrapper,
  FLexTabTopImage,
  FlexCenter,
  AccountNumber,
  CaretImage,
  AccountType,
  AccountBalance,
} from "./styles";

export const ListCard = ({ imgSource, listTitle, imgHeight, imgWidth }) => {
  return (
    <Wrapper>
      <FLexTabTopImage
        source={imgSource}
        style={{ width: imgWidth, height: imgHeight }}
      />
      <FlexCenter style={{borderBottomWidth: 1, borderBottomColor:"#EEF0F1"}}>
        <AccountType>{listTitle}</AccountType>
        <CaretImage source={IMAGES.caret} />
      </FlexCenter>
    </Wrapper>
  );
};
