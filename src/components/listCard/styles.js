// Core Packages
import styled from "styled-components/native";
import { COLORS } from "@utils/theme";

export const Wrapper = styled.TouchableOpacity`
  // flex: 1;
  flex-direction: row;
  justify-content: space-between;
  background: ${COLORS.white};
  width: 100%;
  padding: 10px;
`;
export const AccountType = styled.Text`
  font-family: "Nexa";
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  line-height: 16px;
  text-align: left;
  color: #270450;
`;
export const MiniText = styled.Text`
  font-family: "Nexa";
  font-style: normal;
  font-weight: 600;
  font-size: 13px;
  line-height: 16px;
  text-align: left;
  color: #270450;
  margin-top: 3px;
`;
export const ValueText = styled.Text`
  font-family: "Nexa";
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  line-height: 21px;
  text-align: left;
  color: #270450;
`;
export const AccountNumber = styled.Text`
font-family: 'ABeeZee';
font-style: normal;
font-weight: 600;
font-size: 11px;
line-height: 13px;
text-align: left;
color: #9CA0A5;
`;
export const AccountBalance = styled.Text`
font-family: 'ABeeZee';
font-style: normal;
font-size: 28px;
line-height: 34px;
text-align: left;
color: #270450;
`;
export const CaretImage = styled.Image`
  width: 10px;
  height: 14px;
`;
export const FLexTabTopImage = styled.Image`
  width: 40px;
  height: 40px;
`;
export const FlexCenter = styled.View`
  justify-content: space-between;
  flex-direction: row;
  align-items: center;
  flex: 1;
   margin-left: 10px;
   padding-left: 10px;
  //  padding-top: 10px;
   padding-bottom: 20px;

`;
