// Core Packages
import React from 'react';
import styled from 'styled-components/native';

export const SafeArea = styled.SafeAreaView`
  flex: 1;
`;

export default function ({ children, style }) {
  return <SafeArea style={style}>{children}</SafeArea>;
}
