// Core Component
import React, { useEffect, useRef, useState } from 'react';
import { View, TouchableWithoutFeedback } from 'react-native';

// Utils
import {
  ALPHA_REGEX,
  PHONE_REGEX,
  EMAIL_REGEX,
  NUMBER_REGEX,
  CARD_NUMBER_REGEX,
  EXPIRY_REGEX,
} from '@constants';
import {
  formatAmount,
  formatCardNumber,
  getInputError,
  formatExpiry,
  getKeyboardType,
} from '@utils/helper';
import { COLORS } from '@utils/theme';

// Styles
import {
  Label,
  LabelContainer,
  InputWrapper,
  LeftAdornment,
  RightAdornment,
  ErrorText,
  InputField,
} from './styles';

const TextField = ({
  label,
  value,
  onChange = () => {},
  type = 'text', // text, phone, price, cvv, cardNumber, number, alphabet, email, cardExpiry
  placeholder = '',
  disabled = false,
  required = false,
  style,
  height = 50,
  leftPadding = 0,
  rightPadding = 0,
  onBlur,
  onFocus,
  customError,
  requiredErrorMessage = 'This field is required.',
  multiline = false,
  rightAdornment = null, // () => <JSX> </JSX>
  leftAdornment = null, // () => <JSX> </JSX>
  showError = true,
}) => {
  const inputRef = useRef(null);
  const keyboardType = getKeyboardType(type);
  const [isFocused, setIsFocused] = useState(false);
  const [inputValue, setValue] = useState(value || '');
  const [error, setError] = useState(customError);
  const hasError = (error || customError) && showError;

  const validatePhone = val => val.length === 11 && PHONE_REGEX.test(val);
  const validateEmail = val => EMAIL_REGEX.test(val);
  const validateExpiry = val => EXPIRY_REGEX.test(val);
  const validateCard = val => val.length === 16 && CARD_NUMBER_REGEX.test(val);
  const validateCVV = val => val.length === 3;
  const formatPhone = val => (val[0] === '0' ? val : `0${val}`).substr(0, 11);
  const formatCVV = val => val.substr(0, 3);
  const focusOnInput = () => inputRef.current?.focus();

  // Handle onChange for each input variant
  const onValueChange = val => {
    if (type === 'phone') {
      if (!PHONE_REGEX.test(val) && !NUMBER_REGEX.test(val)) {
        return;
      }
      const formattedPhone = val ? formatPhone(val) : val;
      onChange?.(formattedPhone);
      if (error && validatePhone(formattedPhone)) {
        setError(null);
      }
    } else if (type === 'cardExpiry') {
      if (val.indexOf('.') >= 0 || val.length > 5) {
        return;
      }
      if (val && !NUMBER_REGEX.test(val.split('/').join(''))) {
        return;
      }
      const formattedExp = val ? formatExpiry(val) : val;
      onChange?.(formattedExp);

      if (error && validateExpiry(formattedExp)) {
        setError(null);
      }
    } else if (type === 'cvv') {
      if (!NUMBER_REGEX.test(val)) {
        return;
      }
      const formattedCVV = val ? formatCVV(val) : val;
      onChange?.(formattedCVV);

      if (error && validateCVV(formattedCVV)) {
        setError(null);
      }
    } else if (type === 'cardNumber') {
      const actualVal = String(val).split(' ').join('');
      if (!NUMBER_REGEX.test(actualVal) || actualVal.length > 16) {
        return;
      }
      const formattedValue = actualVal
        ? formatCardNumber(actualVal)
        : actualVal;
      onChange?.(formattedValue || '');
      if (error && validateCard(actualVal)) {
        setError(null);
      }
    } else if (type === 'price') {
      const actualVal = Number(String(val).split(',').join(''));
      if (!NUMBER_REGEX.test(actualVal)) {
        return;
      }
      const formattedAmount = actualVal
        ? formatAmount(actualVal, true)
        : actualVal;
      onChange?.(formattedAmount);
    } else if (type === 'number') {
      if (!NUMBER_REGEX.test(val)) {
        return;
      }
      onChange?.(val);
    } else if (type === 'email') {
      onChange?.(val.toLowerCase());
      if (error && validateEmail(val.toLowerCase())) {
        setError(null);
      }
    } else if (type === 'alphabet') {
      if (val && !ALPHA_REGEX.test(val)) {
        return;
      }
      onChange?.(val || '');
    } else {
      onChange?.(val);
    }
  };

  // Update error display if necessary and set Focused state to false
  const onInputBlur = event => {
    setIsFocused(false);
    if (required && !inputValue) {
      setError(requiredErrorMessage);
    }
    if (inputValue) {
      setError(getInputError(inputValue, type));
    }
    onBlur?.(event);
  };

  // Set Focus state to true when input is focused
  const onInputFocus = event => {
    setIsFocused(true);
    onFocus?.(event);
  };

  // Update input and error display when props change
  useEffect(() => {
    setValue(value || '');
    if (value && error && error === requiredErrorMessage) {
      setError('');
    }
  }, [error, requiredErrorMessage, value]);

  useEffect(() => setError(customError), [customError]);

  // Focused, Disabled and Error states styling
  let color = isFocused ? COLORS.btnDark : COLORS.lightgrey;
  let setBorderWidth = isFocused ? 1 : 0;
  if (error && showError) {
    color = COLORS.alerzoRed;
    setBorderWidth = 1;
  }
  const inputStyle = {
    height,
    paddingRight: rightAdornment ? leftPadding || 54 : 10,
    paddingLeft: leftAdornment ? rightPadding || 54 : 10,
    borderColor: color,
    borderWidth: setBorderWidth,
    color: !value || disabled ? COLORS.greyTwo : COLORS.btnDark,
    backgroundColor: disabled || isFocused ? 'transparent' : COLORS.offWhite,
  };

  return (
    <View style={[style, { marginBottom: 8 }]}>
      {label ? (
        <TouchableWithoutFeedback onPress={focusOnInput}>
          <LabelContainer>
            <Label>{hasError ? `${label}*` : label}</Label>
          </LabelContainer>
        </TouchableWithoutFeedback>
      ) : null}
      <InputWrapper>
        {leftAdornment ? (
          <LeftAdornment hasError={hasError}>{leftAdornment?.()}</LeftAdornment>
        ) : null}
        <InputField
          style={inputStyle}
          ref={inputRef}
          multiline={multiline}
          value={value}
          keyboardType={keyboardType}
          onBlur={onInputBlur}
          onChangeText={onValueChange}
          onFocus={onInputFocus}
          editable={!disabled}
          selectTextOnFocus={!disabled}
          placeholder={placeholder}
          placeholderTextColor={COLORS.fadeBlack}
        />
        {rightAdornment ? (
          <RightAdornment hasError={hasError}>
            {rightAdornment?.()}
          </RightAdornment>
        ) : null}
      </InputWrapper>
      {hasError ? <ErrorText>{customError || error}</ErrorText> : null}
    </View>
  );
};

export default TextField;
