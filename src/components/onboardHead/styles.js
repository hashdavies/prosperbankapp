// Core Packages
import styled from "styled-components/native";

// Utils
import { COLORS, SPACING, FONT_FAMILY } from "@utils/theme";

export const OnBoardContainer = styled.View`
  width: 100%;
  display: flex;
  backgroundColor: #270450;
  flex-direction: row;
  align-items: center;
  justify-content : space-between;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-right: 40px;
  padding-left: 20px;
`;
export const GroupView = styled.View`
 display: flex
 align-Items: flex-start;
 justify-content: center

`;
export const ActionWrap = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
`;
export const Title = styled.Text`
  font-style: normal;
  font-weight: 500;
  font-size: 24px;
  color: ${COLORS.btnDark};
`;
export const Image = styled.Image`
  width: 24px;
  height: 24px;
`;
export const SubTitle = styled.Text`
  font-family: "ABeeZee";
  margin-top: 3px;
  margin-bottom: 20px;
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  color: ${COLORS.fadeBlack};
`;
export const RHSView = styled.View`
  
`;
export const Cnt_View = styled.View`
align-items: center;
  
`;
export const Label = styled.Text`
  font-size: 16px;
  font-family: ${FONT_FAMILY.Gilmer500};
  width: 100%;
  color: ${({ active }) => (active ? COLORS.greyBlue : COLORS.white)};
  text-align: center;
`;
