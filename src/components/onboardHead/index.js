// Core Packages
import React from "react";

// Styles
import {
  OnBoardContainer,
  GroupView,
  ActionWrap,
  Title,
  SubTitle,
  Cnt_View,
  Image,
  RHSView,
} from "./styles";

export const OnBoardHead = ({
  title,
  subTitle,
  imageLink,
  handleOnPress,
  RHS_children,
  bgColor,
  style,
}) => {
  return (
    <OnBoardContainer style={[style, { backgroundColor: bgColor || "#270450" }]}>
      <GroupView>
        {imageLink && (
          <ActionWrap onPress={handleOnPress}>
            <Image source={imageLink} />
          </ActionWrap>
        )}
      </GroupView>
      <Cnt_View>
        <Title>{title}</Title>
        <SubTitle>{subTitle}</SubTitle>
      </Cnt_View>

      <RHSView>{RHS_children}</RHSView>
    </OnBoardContainer>
  );
};
