import styled from "styled-components/native";
import { COLORS } from "@utils/theme";

export const InputWrapper = styled.View`
  display: flex;
  flex-direction: row;
  background: ${({ isFocused }) =>
    isFocused ? `${COLORS.transparent}` : `${COLORS.offWhite}`};

  border: 0.8px solid
    ${({ isFocused }) =>
      isFocused ? `${COLORS.btnDark}` : `${COLORS.transparent}`};
  box-sizing: border-box;
  border-radius: 10px;
  height: 48px;
  margin-bottom: 25px;
`;

export const StyledInput = styled.TextInput`
  flex: 0.9;
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  line-height: 14px;
  color: ${COLORS.cardDark};
`;

export const ImageWrapper = styled.TouchableOpacity`
  flex: 0.1;
  align-items: center;
  justify-content: center;
`;
