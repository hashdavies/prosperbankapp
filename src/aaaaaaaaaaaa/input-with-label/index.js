import React, { useState } from 'react';
import { Image } from 'react-native';
import { COLORS } from '@utils/theme';

// Utils
import { InputWrapper, StyledInput, ImageWrapper } from './styles';

export const InputWithLabel = props => {
  const [isFocused, setIsFocused] = useState(false);
  return (
    <InputWrapper isFocused={isFocused}>
      <StyledInput
        placeholderTextColor={COLORS.fadeBlack}
        onFocus={() => {
          setIsFocused(true);
        }}
        onBlur={() => {
          setIsFocused(false);
        }}
        {...props}
      />
      <ImageWrapper onPress={props.onPress}>
        <Image source={props.source} style={{ width: 24, height: 24 }} />
      </ImageWrapper>
    </InputWrapper>
  );
};
