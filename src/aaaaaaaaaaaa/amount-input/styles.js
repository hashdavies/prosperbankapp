import styled from 'styled-components/native';
import { COLORS } from '@utils/theme';

export const AmountInputWrapper = styled.View`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 15px;
`;
export const AmountInputLabel = styled.Text`
  font-family: 'GilmerRegular';
  font-size: 14px;
  line-height: 17px;
  text-align: center;
  color: ${COLORS.fadeGrey2};
  margin-bottom: 25px;
`;
export const AmountAvailable = styled.Text`
  font-family: 'GilmerRegular';
  font-size: 12px;
  line-height: 14px;
  text-align: center;
  color: ${COLORS.accountNumber};
  margin-bottom: 15px;
`;
export const InsufficientBalance = styled.Text`
  font-family: 'GilmerRegular';
  font-size: 12px;
  line-height: 14px;
  text-align: center;
  color: ${COLORS.alerzoRed};
  margin-bottom: 21px;
`;
export const AmountCurrencyWrapper = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
export const CurrencyText = styled.Text`
  font-family: 'GilmerRegular';
  font-style: normal;
  font-weight: 600;
  font-size: 36px;
  line-height: 43px;
  text-align: center;
  color: ${COLORS.alerzoGreyFive};
`;

export const StyledInput = styled.TextInput`
  font-family: 'GilmerRegular';
  font-style: normal;
  font-weight: 600;
  font-size: 36px;
  line-height: 43px;
  text-align: center;
  color: ${COLORS.cardDark}; ;
`;
