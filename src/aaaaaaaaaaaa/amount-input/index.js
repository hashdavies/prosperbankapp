import React, { useState } from 'react';
import { Image } from 'react-native';

// Utils
import {
  AmountInputWrapper,
  AmountInputLabel,
  AmountAvailable,
  StyledInput,
  CurrencyText,
  AmountCurrencyWrapper,
  InsufficientBalance,
} from './styles';

export const AmountInput = props => {
  const [isFocused, setIsFocused] = useState(false);
  const focusInput = () => setIsFocused(true);
  const unfocusInput = () => setIsFocused(false);

  return (
    <AmountInputWrapper isFocused={isFocused}>
      <AmountInputLabel>{props.amountInputLabel}</AmountInputLabel>
      <AmountCurrencyWrapper>
        <CurrencyText>{props.amountCurrency}</CurrencyText>

        <StyledInput onFocus={focusInput} onBlur={unfocusInput} {...props} />
      </AmountCurrencyWrapper>

      <AmountAvailable>{props.amountAvailable}</AmountAvailable>
      <InsufficientBalance>{props.insurficientBalText}</InsufficientBalance>
    </AmountInputWrapper>
  );
};
