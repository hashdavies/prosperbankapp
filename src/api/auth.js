// Core Packages
import apisauce from 'apisauce';
import { ALERZO_BASE_URL } from '@env';
// utils
import { parseApiProblems } from '@utils/helper';

const api = apisauce.create({
  baseURL: ALERZO_BASE_URL,
  timeout: 100000,
  headers: {
    'Cache-Control': 'no-cache',
    Accept: 'application/json',
    ContentType: 'application/json',
    'x-auth-group': 'customer',
    // Authorization: `Bearer ${token}`
  },
});

const processResponse = async response => {
  if (!response.ok) {
    const problem = parseApiProblems(response);
    if (problem) {
      return problem;
    }
  }
  return { kind: 'ok', data: response.data };
};

export const signInUser = async code => {
  const response = await api.post('/customer/app/login/oauth', { code });
  return processResponse(response);
};
