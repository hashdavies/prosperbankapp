// Core Packages
import { Linking } from 'react-native';
import { InAppBrowser } from 'react-native-inappbrowser-reborn';
import moment from 'moment';

// Utils
import {
  EXPIRY_REGEX,
  EMAIL_REGEX,
  NUMBER_REGEX,
  CARD_NUMBER_REGEX,
} from '@constants';

export const formatAmount = (amount, toWholeNumber = false) => {
  if (toWholeNumber) {
    const value = Math.round(Number(amount));
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }
  let formattedAmount = (+amount || 0).toFixed(2).toString();
  if (!formattedAmount.includes('.')) {
    formattedAmount = `${amount}.00`;
  }
  return formattedAmount.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

export const formatCardNumber = cardNumber => {
  const value = Math.round(Number(cardNumber)).toString();
  var format = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
  var matches = format.match(/\d{4,16}/g);
  var match = (matches && matches[0]) || '';
  var parts = [];
  for (let i = 0; i < match.length; i += 4) {
    parts.push(match.substring(i, i + 4));
  }
  if (parts.length) {
    return parts.join(' ');
  } else {
    return value;
  }
};

export const formatExpiry = text => {
  if (text.length === 2) {
    let newText = text.split('/').join('');
    return (newText += '/');
  }
  return text;
};

export const formatAsInteger = amount =>
  parseInt(String(amount).split(',').split(' ').join(''), 10);

export const formatQuantity = quantity =>
  String(quantity).replace(/\B(?=(\d{3})+(?!\d))/g, ',');

export const getAllUrlParams = url => {
  // get query string from url (optional) or window
  var queryString = url ? url.split('?')[1] : window.location.search.slice(1);
  // we'll store the parameters here
  var obj = {};
  // if query string exists
  if (queryString) {
    // stuff after # is not part of query string, so get rid of it
    queryString = queryString.split('#')[0];
    // split our query string into its component parts
    var arr = queryString.split('&');
    for (var i = 0; i < arr.length; i++) {
      // separate the keys and the values
      var a = arr[i].split('=');
      // set parameter name and value (use 'true' if empty)
      var paramName = a[0];
      var paramValue = typeof a[1] === 'undefined' ? true : a[1];
      // if the paramName ends with square brackets, e.g. colors[] or colors[2]
      if (paramName.match(/\[(\d+)?\]$/)) {
        // create key if it doesn't exist
        var key = paramName.replace(/\[(\d+)?\]/, '');
        if (!obj[key]) {
          obj[key] = [];
        }
        // if it's an indexed array e.g. colors[2]
        if (paramName.match(/\[\d+\]$/)) {
          // get the index value and add the entry at the appropriate position
          var index = /\[(\d+)\]/.exec(paramName)[1];
          obj[key][index] = paramValue;
        } else {
          // otherwise add the value to the end of the array
          obj[key].push(paramValue);
        }
      } else {
        // we're dealing with a string
        if (!obj[paramName]) {
          // if it doesn't exist, create property
          obj[paramName] = paramValue;
        } else if (obj[paramName] && typeof obj[paramName] === 'string') {
          // if property does exist and it's a string, convert it to an array
          obj[paramName] = [obj[paramName]];
          obj[paramName].push(paramValue);
        } else {
          // otherwise add the property
          obj[paramName].push(paramValue);
        }
      }
    }
  }
  return obj;
};

export const getGreeting = options => {
  const { iconOnly = true } = options || {};
  const date = new Date();
  const time = date.getHours();

  if (time < 12) {
    return iconOnly ? '☕️' : 'Good morning ☕️';
  }
  if (time < 17) {
    return iconOnly ? '☀️' : 'Good afternoon ☀️';
  }
  return iconOnly ? '🌙' : 'Good evening 🌙';
};

export const formatDate = (dateString, divider = '-') => {
  const date = new Date(Number(dateString) || Date.now());
  const year = date.getFullYear();
  const month = date.getMonth().toString().padStart(2, '0');
  const day = date.getDate().toString().padStart(2, '0');
  return `${day}${divider}${month}${divider}${year}`;
};

export const getStartAndEndDate = daysBack => {
  const currentDate = moment();
  const startDate = currentDate
    .clone()
    .subtract(daysBack - 1, 'days')
    .startOf('day')
    .format('YYYY-MM-DD');
  const endDate = currentDate
    .clone()
    .add(1, 'days')
    .endOf('day')
    .format('YYYY-MM-DD');
  const displayStartDate = currentDate
    .clone()
    .subtract(daysBack - 1, 'days')
    .startOf('day')
    .format('DD/MM/YYYY');
  const displayEndDate = currentDate.clone().endOf('day').format('DD/MM/YYYY');
  return { startDate, endDate, displayStartDate, displayEndDate };
};

export const getMostRecentDate = timeFrame => {
  const currentDate = moment();
  const start = currentDate.clone().startOf(timeFrame);
  const end = currentDate.clone().endOf(timeFrame);

  if (currentDate === end || timeFrame === 'day') {
    const startDate = start.clone().format('YYYY-MM-DD');
    const endDate = end.clone().add(1, 'days').format('YYYY-MM-DD');
    const displayStartDate = start.clone().format('DD/MM/YYYY');
    const displayEndDate = end.clone().format('DD/MM/YYYY');
    return { startDate, endDate, displayStartDate, displayEndDate };
  } else {
    const startDate = start
      .clone()
      .subtract(1, `${timeFrame}s`)
      .format('YYYY-MM-DD');
    const endDate = end
      .clone()
      .add(1, 'days')
      .subtract(1, `${timeFrame}s`)
      .format('YYYY-MM-DD');
    const displayStartDate = start
      .clone()
      .subtract(1, `${timeFrame}s`)
      .format('DD/MM/YYYY');
    const displayEndDate = end
      .clone()
      .subtract(1, `${timeFrame}s`)
      .format('DD/MM/YYYY');
    return { startDate, endDate, displayStartDate, displayEndDate };
  }
};

export const mergeDateAndTime = (date, time) =>
  new Date(
    date.getFullYear(),
    date.getMonth(),
    date.getDate(),
    time.getHours(),
    time.getMinutes(),
    time.getSeconds(),
  );

export const isURL = str => {
  const pattern = new RegExp(
    '^(https?:\\/\\/)?' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$',
    'i',
  ); // fragment locator
  return !!pattern.test(str);
};

export const getTomorrowDate = () => {
  const tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  return tomorrow;
};

export const getYesterdayDate = () => {
  const yesterday = new Date();
  yesterday.setDate(yesterday.getDate() - 1);
  return yesterday.toISOString();
};

// export const getPastDate = days => {
//   const yesterday = new Date();
//   yesterday.setDate(yesterday.getDate() - days);
//   return yesterday.toISOString();
// };

// export const splitPhoneAndCode = phoneNumber => {
//   const num = String(phoneNumber || '');
//   if (num.startsWith('+') && num.length > 11) {
//     return {
//       phone: `0${num.substr(num.length - 10)}`,
//       prefix: num.substr(0, num.length - 10).substring(1),
//     };
//   } else if (!num.startsWith('+') && num.length > 11) {
//     return {
//       phone: `0${num.substr(num.length - 10)}`,
//       prefix: num.substr(0, num.length - 10),
//     };
//   }
//   return { phone: num, prefix: '' };
// };

// export const mergePhoneAndCode = (countryCode, phoneNumber) => {
//   const code = String(countryCode || '');
//   const num = String(phoneNumber || '');
//   if (!code || !num) {
//     return num;
//   } else if (code.startsWith('+') && num.startsWith('0')) {
//     return `${code}${num.substring(1)}`;
//   } else if (!code.startsWith('+') && num.startsWith('0')) {
//     return `+${code}${num.substring(1)}`;
//   }
//   return num;
// };

// export const getTruncatedString = (str, num) => {
//   if (str?.length <= num || num === -1) {
//     return str;
//   }
//   return `${str?.slice(0, num)}...`;
// };

// export const openLink = async url => {
//   try {
//     if (await InAppBrowser.isAvailable()) {
//       const result = await InAppBrowser.open(url, {
//         // iOS Properties
//         dismissButtonStyle: 'cancel',
//         preferredBarTintColor: '#0077FF',
//         preferredControlTintColor: 'white',
//         readerMode: false,
//         animated: true,
//         modalPresentationStyle: 'fullScreen',
//         modalTransitionStyle: 'coverVertical',
//         modalEnabled: true,
//         enableBarCollapsing: false,
//         // Android Properties
//         showTitle: true,
//         toolbarColor: '#0077FF',
//         secondaryToolbarColor: 'black',
//         navigationBarColor: 'black',
//         navigationBarDividerColor: 'white',
//         enableUrlBarHiding: true,
//         enableDefaultShare: true,
//         forceCloseOnRedirection: false,
//         // Specify full animation resource identifier(package:anim/name)
//         // or only resource name(in case of animation bundled with app).
//         animations: {
//           startEnter: 'slide_in_right',
//           startExit: 'slide_out_left',
//           endEnter: 'slide_in_left',
//           endExit: 'slide_out_right',
//         },
//         headers: {
//           'my-custom-header': 'my custom header value',
//         },
//       });
//     } else {
//       Linking.openURL(url);
//     }
//   } catch (error) {
//     console.log(error.message);
//   }
// };

// export const secondsElapsed = (date1, date2) => {
//   const dif = date1.getTime() - date2.getTime();
//   const Seconds_from_date1_to_date2 = dif / 1000;
//   return Math.abs(Seconds_from_date1_to_date2);
// };

// export const parseApiProblems = response => {
//   switch (response.problem) {
//     case 'CONNECTION_ERROR':
//       return { kind: 'cannot-connect', temporary: true };
//     case 'NETWORK_ERROR':
//       return { kind: 'cannot-connect', temporary: true };
//     case 'TIMEOUT_ERROR':
//       return { kind: 'timeout', temporary: true };
//     case 'SERVER_ERROR':
//       return { kind: 'server' };
//     case 'UNKNOWN_ERROR':
//       return { kind: 'unknown', temporary: true };
//     case 'CLIENT_ERROR':
//       const { status, data } = response;
//       switch (status) {
//         case 401:
//           return { kind: 'unauthorized', data: data };
//         case 403:
//           return { kind: 'forbidden', data: data };
//         case 404:
//           return { kind: 'not-found', data: data };
//         default:
//           return { kind: 'rejected', data: data };
//       }
//     case 'CANCEL_ERROR':
//       return null;
//   }
//   return null;
// };

// export const getInputError = (value, type) => {
//   if (type === 'phone' && value && value.length !== 11) {
//     return 'Invalid phone number';
//   }
//   if (type === 'cvv' && value && value.length !== 3) {
//     return 'Invalid CVV';
//   }
//   if (type === 'email' && value && !EMAIL_REGEX.test(value)) {
//     return 'Invalid email address';
//   }
//   if (type === 'cardExpiry' && value && !EXPIRY_REGEX.test(value)) {
//     return 'Invalid card expiry';
//   }
//   if (
//     type === 'card' &&
//     value &&
//     !CARD_NUMBER_REGEX.test(value.split(' ').join(''))
//   ) {
//     return 'Invalid card number';
//   }
//   if (
//     type === 'price' &&
//     value &&
//     !NUMBER_REGEX.test(String(value).split(',').join(''))
//   ) {
//     return 'Invalid value';
//   }
//   return '';
// };

// export const getKeyboardType = variant => {
//   let result = 'default';
//   switch (variant) {
//     case 'phone':
//     case 'price':
//     case 'cvv':
//     case 'cardNumber':
//     case 'cardExpiry':
//     case 'number':
//       result = 'numeric';
//       break;
//     case 'email':
//       result = 'email-address';
//       break;
//     case 'alphabet':
//     default:
//       result = 'default';
//   }
//   return result;
// };
