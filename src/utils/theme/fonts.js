import * as Font from 'expo-font';

export const loadFontsAsync = () =>
  Font.loadAsync({
    GilmerLight: require('@assets/fonts/GilmerLight.otf'),
    GilmerBold: require('@assets/fonts/GilmerBold.otf'),
    GilmerHeavy: require('@assets/fonts/GilmerHeavy.otf'),
    GilmerMedium: require('@assets/fonts/GilmerMedium.otf'),
    GilmerOutline: require('@assets/fonts/GilmerOutline.otf'),
    GilmerRegular: require('@assets/fonts/GilmerRegular.otf'),
  });

export const FONTS = {
  FilsonProRegular: require('@assets/fonts/FilsonProRegular.ttf'),
  FilsonProBold: require('@assets/fonts/FilsonProBold.ttf'),
  GilmerLight: require('@assets/fonts/GilmerLight.otf'),
  GilmerBold: require('@assets/fonts/GilmerBold.otf'),
  GilmerHeavy: require('@assets/fonts/GilmerHeavy.otf'),
  GilmerMedium: require('@assets/fonts/GilmerMedium.otf'),
  GilmerOutline: require('@assets/fonts/GilmerOutline.otf'),
  GilmerRegular: require('@assets/fonts/GilmerRegular.otf'),
};

export const FONT_FAMILY = {
  Gilmer700: 'GilmerHeavy',
  Gilmer600: 'GilmerBold',
  Gilmer500: 'GilmerMedium',
  Gilmer400: 'GilmerRegular',
  Gilmer300: 'GilmerLight',
};
