import { COLORS } from 'src/utils/theme/colors';
import { FONT_FAMILY } from 'src/utils/theme/fonts';
import { SPACING } from 'src/utils/theme/spacing';

export { COLORS, SPACING, FONT_FAMILY };
