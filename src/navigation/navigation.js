// Core Packages
import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

// Screens
import { OnBoarding } from '@screens/onboarding/index';
import { History } from '@screens/history/index';
import { CurrentAccount } from '@screens/currentAccount/index';

// Custom Components
import { ROUTE_NAMES } from './route-names';

// Utils
import { COLORS, FONT_FAMILY } from '@utils/theme';

const Stack = createNativeStackNavigator();

export const MainStack = () => (
  <Stack.Navigator
    screenOptions={{
      gestureEnabled: false,
      headerTintColor: COLORS.greyBlue,
      headerStyle: {
        backgroundColor: COLORS.lightBlueFive,
      },
      headerTitleStyle: {
        fontFamily: FONT_FAMILY.Gilmer600,
        fontSize: 16,
      },
    }}
    initialRouteName={ROUTE_NAMES.ON_BOARDING}>
    <Stack.Screen
      name={ROUTE_NAMES.ON_BOARDING}
      component={OnBoarding}
      options={{ headerShown: false }}
    />

    <Stack.Screen
      name={ROUTE_NAMES.HISTORY}
      component={History}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name={ROUTE_NAMES.CURRENT_ACCOUNT}
      component={CurrentAccount}
      options={{ headerShown: false }}
    />
  </Stack.Navigator>
);
