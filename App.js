// Core Packages
import React, { useEffect, useState } from 'react';
import SplashScreen from 'react-native-splash-screen';
import { NavigationContainer } from '@react-navigation/native';
import 'react-native-gesture-handler';

// Custom Components
import { Loader } from '@components/loader';

// Utils
import { FONTS } from '@utils/theme/fonts';
import { useFonts } from 'expo-font';
import { MainStack } from './src/navigation/navigation';
import { StatusBar } from 'react-native';

const App = () => {
  const [loaded, setLoaded] = useState(false);
  const closeAnimatedSplashScreen = () => setLoaded(true);
  useEffect(() => SplashScreen.hide(), []);
  const [isReady] = useFonts({ ...FONTS });

  if (!isReady) {
    return null;
  }

  return (
    <>
      <StatusBar barStyle="light-content" />
      <NavigationContainer fallback={<Loader />}>
        <MainStack />
      </NavigationContainer>
    </>
  );
};

export default App;
